import happi
import numpy as np
import matplotlib.pyplot as plt
from numpy.fft import fft
import pandas as pd
###############################################################
###############################################################
S = happi.Open("./output")

every = 1632
Tmax = 65280

#for i in range(int(Tmax/every)):
#    res = np.array(S.Field(0, field='Ex').getData(int(i*every))

res = np.array(S.Field(0, field='Ex').getData())
np.savetxt("Ex.txt", res)
print(res.shape)

res = np.array(S.Field(0, field='Ey').getData())
np.savetxt("Ey.txt", res)
print(res.shape)

res = np.array(S.Field(0, field='Ez').getData())
np.savetxt("Ez.txt", res)
print(res.shape)

#res = np.array(S.ParticleBinning(0).getData())
#np.savetxt("Weight.txt", res)
#print(res.shape)

#res = np.array(S.ParticleBinning(1).getData())
#np.savetxt("Px.txt", res)
#print(res.shape)

#res = np.array(S.ParticleBinning(2).getData())
#np.savetxt("Py.txt", res)
#print(res.shape)

#res = np.array(S.ParticleBinning(3).getData())
#np.savetxt("Pz.txt", res)
#print(res.shape)

#res = np.array(list(S.TrackParticles('eon', axes = ['px']).getData().items()), dtype=object)
#res = np.concatenate((res[0][1][:, np.newaxis].T, res[1][1].T))
#np.savetxt("Track_px.txt", res)
#print(res.shape)

#res = np.array(list(S.TrackParticles('eon', axes = ['py']).getData().items()), dtype=object)
#res = np.concatenate((res[0][1][:, np.newaxis].T, res[1][1].T))
#np.savetxt("Track_py.txt", res)
#print(res.shape)

#res = np.array(list(S.TrackParticles('eon', axes = ['pz']).getData().items()), dtype=object)
#res = np.concatenate((res[0][1][:, np.newaxis].T, res[1][1].T))
#np.savetxt("Track_pz.txt", res)
#print(res.shape)

#res = np.array(list(S.TrackParticles('eon', axes = ['x']).getData().items()), dtype=object)
#res = np.concatenate((res[0][1][:, np.newaxis].T, res[1][1].T))
#np.savetxt("Track_x.txt", res)
#print(res.shape)

#res = np.array(list(S.TrackParticles('eon', axes = ['w']).getData().items()), dtype=object)
#res = np.concatenate((res[0][1][:, np.newaxis].T, res[1][1].T))
#np.savetxt("Track_w.txt", res)
#print(res.shape)

#S.ParticleBinning(0).animate(movie="particle_bin_px.mp4")#, vmin = -1, vmax = 1)
#S.ParticleBinning(1).animate(movie="particle_bin_py.mp4")#, vmin = -1, vmax = 1)
#S.ParticleBinning(2).animate(movie="particle_bin_pz.mp4")#, vmin = -1, vmax = 1)

#S.TrackParticles('eon', axes = ['px']).animate(movie="particles.mp4")
###############################################################
###############################################################
#S.Field(0, field='Ey').plot(timestep=450, saveAs="slice1.png")
#S.Field(1, field='Ey').plot(timestep=450, saveAs="slice2.png")
#S.Field(2, field='Ey').plot(timestep=450, saveAs="data.png")

#S.Field(0, field='Ex').animate(movie="mirror_ex.mp4", vmax=25.0, vmin=-25.0)
#S.Field(0, field='Ey').animate(movie="mirror_ey.mp4", vmax=25.0, vmin=-25.0)
#S.Field(0, field='Ez').animate(movie="mirror_ez.mp4", vmax=25.0, vmin=-25.0)
#S.Field(0, field='Bx_m').animate(movie="mirror_bx_m.mp4", vmax=5.0, vmin=-5.0)
#S.Field(0, field='By_m').animate(movie="mirror_by_m.mp4", vmax=5.0, vmin=-5.0)
#S.Field(0, field='Bz_m').animate(movie="mirror_bz_m.mp4", vmax=5.0, vmin=-5.0)

###############################################################
#S.Scalar("Utot").streak(saveAs="data.png")
###############################################################
#res = np.array(S.Field(0, field='Ey').getData(1140))
#print(res.shape)
#plt.imshow(res[0])
#plt.imshow(res[0][225:250][:])
#plt.savefig("res.png")
###############################################################
#for i in range(100):
#    res =  np.array(S.Field(0, field='Ey').getData(int(i*15)))
#    N = res[0][225].shape[0]
#    plt.plot(fft(res[0][225])[N//2:N])
#    plt.ylabel("Ey")
#    plt.ylim(0, 6)
#    filename = "res_%d" % i
#    plt.savefig(filename)
#    plt.close()
###############################################################
