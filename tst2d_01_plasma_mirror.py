# ----------------------------------------------------------------------------------------
# 					SIMULATION PARAMETERS FOR THE PIC-CODE SMILEI
# ----------------------------------------------------------------------------------------

import math

l0 = 2.*math.pi			# laser wavelength
t0 = l0					# optical cycle
Xmax = 20.*l0
Ymax = 30.*l0
Lsim = [Xmax, Ymax]	# length of the simulation
Tsim = 25.*t0			# duration of the simulation
resx = 200.			# nb of cells in on laser wavelength
resy = 50.
rest = 216.			# time of timestep in one optical cycle 


def sin_square_envelope(time):
    if time >= 0.0 and time < 4.0*t0:
        return math.sin(math.pi*time/(4.0*t0))**2
    else:
        return 0.0

def space_envelope(y):
    
    return 20.0*math.exp(-(y-10*l0)**16/(30*l0))


Main(
	geometry = "2Dcartesian",
	
	interpolation_order = 2 ,
	
	cell_length = [l0/resx,l0/resy],
	grid_length  = Lsim,
	
	number_of_patches = [ 8, 4 ],
	patch_arrangement = "linearized_XY",
	
	timestep = t0/rest,
	simulation_time = Tsim,
	
    print_every = rest,
 
	EM_boundary_conditions = [
		['silver-muller'],
        ['silver-muller'],
		#['reflective'],
	],
	
	random_seed = smilei_mpi_rank
)

#Laser(
#    box_side = "xmin",
#    time_envelope  = sin_square_envelope,
#    space_envelope = [20., 20.],#[space_envelope, space_envelope],
#    phase = [0, 0],
#)

LaserGaussian2D(
	box_side         = "xmin",
	a0              = 20.0,
	focus           = [11.*l0, 15.0*l0],
	waist           = 5.*l0,
	incidence_angle = 0./180.*math.pi,
	time_envelope   = sin_square_envelope,
   ellipticity = 0.,
   polarization_phi = math.pi/4.
)

Species(
	name = 'ion',
	position_initialization = 'random',
	momentum_initialization = 'cold',
	ionization_model = 'none',
	particles_per_cell = 100,
	c_part_max = 1.0,
	mass = 1836.0,
	charge = 1.0,
	number_density = trapezoidal(800.0,xvacuum=11.*l0,xplateau=0.01*l0),
	time_frozen = Tsim,
	boundary_conditions = [
		["reflective", "reflective"],
		["periodic", "periodic"],
	],
)

Species(
	name = 'eon',
	position_initialization = 'random',
	momentum_initialization = 'cold',
	ionization_model = 'none',
	particles_per_cell = 100,
	c_part_max = 1.0,
	mass = 1.0,
	charge = -1.0,
	number_density = trapezoidal(800.0,xvacuum=11.*l0,xplateau=0.01*l0),
	time_frozen = 0.,
	boundary_conditions = [
		["reflective", "reflective"],
		["periodic", "periodic"],
	],
)

ExternalField(
    field = 'Bx',
    profile = constant(0.)
)

#LoadBalancing(
#    every = 1000
#)

from numpy import s_
#DiagFields(
#    every = 100,
#    fields = ['Ex','Ey','Ez'],
#    subgrid = s_[5000, 25]
#)


#DiagFields(
#    every = 100,
#    fields = ['Ex','Ey','Ez'],
#    subgrid = s_[5000, 33]
#)


#DiagFields(
#    every = 100,
#    fields = ['Ey', 'Ez'],
#)

DiagFields(
    every = 1,
    fields = ['Ey','Ez'],
    subgrid = s_[1500, 750]
)

#DiagFields(
#    every = 10,
#    fields = ['Ex','Ey','Ez'],
#    subgrid = s_[2000, 750]
#)

#DiagTrackParticles(
#    species = 'eon',
#    every = 10,
#    attributes = ['x', 'y']#, 'px', 'py', 'pz']
#)


#DiagParticleBinning(
#    deposited_quantity = "weight",
#    every = 200,
#    species = ['eon'],
#    time_average = 1.,
#    axes = [
#        ['x', Xmax/2-0.05*Xmax, Xmax/2+0.005*Xmax, 300],
#        ['y', 0.0, Ymax, 150]
#    ]
#)
