#!/bin/bash -l
#SBATCH -N 1
#SBATCH -n 8
##SBATCH --ntasks-per-node=16
##SBATCH --time=0-0:5:00
##SBATCH --mem=100000
#SBATCH -p htc


export PATH=/trinity/home/r.zagidullin/miniconda3/bin:$PATH
export PATH=/trinity/home/r.zagidullin/miniconda3/condabin:$PATH

# THIS IS FOR SMILEI
#######################################
#######################################
module load compilers/intel_2020.2.254
module load compilers/fftw-3.3.8
module load intel/mkl/2020
module load intel/2020
module load compilers/gcc-7.3.0
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/trinity/home/r.zagidullin/phdf5/lib
export PYTHONEXE=python3
#######################################
#######################################

rm -r output
#rm *mp4

#source ./scripts/set_omp_env.sh 8
#export OMP_NUM_THREADS=8
#export OMP_SCHEDULE=static
#./smilei.sh 8 3d_test.py -o output
#./smilei.sh 16  tst2d_01_plasma_mirror.py -o output
./smilei.sh 8 tst1d_00_em_propagation.py -o output
python3 vis.py
#python3 2d_vis.py
#python3 graphs.py
