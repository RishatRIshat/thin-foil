import math

l0 = 2.0*math.pi  # wavelength in normalized units
t0 = l0           # optical cycle in normalized units
rest = 816.0*4      # nb of timestep in 1 optical cycle
resx = 800.0      # nb cells in 1 wavelength
Tmax = 30.0*t0
Xmax = 30.0*l0

def sin_square_envelope(time):
    if time >= 0.0 and time < 4.0*t0:
        return math.sin(math.pi*time/(4.0*t0))**2
    else:
        return 0.0

Main(
    geometry = "1Dcartesian",
    interpolation_order = 2,
    
    cell_length = [l0/resx],
    grid_length  = [Xmax],
    
    number_of_patches = [ 64 ],
    
    timestep = t0/rest,
    simulation_time = Tmax,
    
    EM_boundary_conditions = [ ['silver-muller'] ],
    
    random_seed = smilei_mpi_rank,
    
    print_every = 1000
)

#some weird shifting occurs here
Laser(
    box_side = "xmin",
    time_envelope  = sin_square_envelope,
    space_envelope = [20., 20.],
    phase = [0, 0],
)

#LaserPlanar1D(
#    box_side = "xmin",
#    time_envelope  = sin_square_envelope,
#    ellipticity = 1.,
#    a0 = 28.28
#)


Species(
	name = 'ion',
	position_initialization = 'random',
	momentum_initialization = 'cold',
	ionization_model = 'none',
	particles_per_cell = 200,
	c_part_max = 1.0,
	mass = 1836.0,
	charge = 1.0,
	number_density = trapezoidal(400.0,xvacuum=11.*l0,xplateau=0.01*l0),
	time_frozen = Tmax,
	boundary_conditions = [
		["reflective", "reflective"],
	],
)

Species(
	name = 'eon',
	position_initialization = 'random',
	momentum_initialization = 'cold',
	ionization_model = 'none',
	particles_per_cell = 200,
	c_part_max = 1.0,
	mass = 1.0,
	charge = -1.0,
	number_density = trapezoidal(400.0,xvacuum=11.*l0,xplateau=0.01*l0),
	time_frozen = 0.,
	boundary_conditions = [
		["reflective", "reflective"],
	],
)


ExternalField(
    field = 'Bx',
    profile = constant(-6.)
)

#DiagScalar(
#    every = 5
#)

#from numpy import s_
#DiagFields(
#    every = int(rest/2.0),
#    fields = ['Ex','Ey','Ez','By_m','Bz_m'],
#    subgrid = s_[4:100:3]
from numpy import s_
DiagFields(
    every = int(rest/1024),
    fields = ['Ex','Ey','Ez'],
    subgrid = s_[1000:1002]
)

#DiagTrackParticles(
#    species = 'eon',
#    every = int(rest/32),
#    attributes = ['x', 'px', 'py', 'pz', 'w']
#)


#DiagParticleBinning(
#    deposited_quantity = "weight",
#    every = int(rest/32),
#    species = ['eon'],
#    time_average = 1.,
#    axes = [ ['x', 0.0, Xmax, 1000]]
#)

#DiagParticleBinning(
#    deposited_quantity = lambda p: p.weight * p.px,
#    every = int(rest/128),
#    species = ['eon'],
#    time_average = 1.,
#    axes = [ ['x', 0.0, Xmax, 1000]]
#)

#DiagParticleBinning(
#    deposited_quantity = lambda p: p.weight * p.py,
#    every = int(rest/128),
#    species = ['eon'],
#    time_average = 1.,
#    axes = [ ['x', 0.0, Xmax, 1000]]
#)

#DiagParticleBinning(
#    deposited_quantity = lambda p: p.weight * p.pz,
#    every = int(rest/128),
#    species = ['eon'],
#    time_average = 1.,
#    axes = [ ['x', 0.0, Xmax, 1000]]
#)

